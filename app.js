const ordenNumber = () => {
  let c = [];
  let minor;
  let array_2 = validateArray();
  for (let i = array_2.length; i > 0; i--) {
    minor = array_2[0];
    for (let i = 0; i < array_2.length; i++) {
      if (array_2[i] < minor) {
        minor = array_2[i];
      }
    }
    c.push(minor);

    array_2.splice(array_2.indexOf(minor), 1);
  }
  document.getElementById("box_1").innerHTML = c;
};
const validateArray = () => {
  let exReg_1 = /^(\s*[0-9][,]?[0-9]*\s*){1,50}$/g;
  let exReg_2 = /\s/g;
  let array = document.getElementById("inputArray").value;
  if (array === "")
    return (document.getElementById("box_1").innerHTML =
      "No has ingresado los números a ordenar");
  if (exReg_1.test(array)) {
    return (array = array.replace(exReg_2, "").split(","));
  }
  document.getElementById("box_1").innerHTML =
    "No debes ingresar letras o simbolos, ingresa números separados por (,)";
};

const parinparNumber = () => {
  let exReg_1 = /^(\s*[0-9][,]?[0-9]*\s*){1,50}$/g;
  let inpar = [];
  let par = [];
  let array_1 = validateArray();
  if (exReg_1.test(array_1)) {
    for (let i = 0; i < array_1.length; i++) {
      if (array_1[i] % 2 === 0) {
        par.push(array_1[i]);
      } else if (array_1[i] % 2 !== 0) {
        inpar.push(array_1[i]);
      }
    }
    document.getElementById("box_2").innerHTML = inpar;
    document.getElementById("box_3").innerHTML = par;
  }
};

const factorialNumber = () => {
  let number_1 = validateFactorial();
  let number_2 = validateFactorial();
  let array_2 = [number_1];
  let result;
  let array = [];
  if (typeof number_1 === "number") {
    for (let i = 0; i < number_1 - 1; i++) {
      let resul = --number_2;
      array.push(resul);
      result = array[i] * array_2[i];
      array_2.push(result);
    }
    document.getElementById("box_5").innerHTML = result;
  }
  return validateFactorial();
};

const validateFactorial = () => {
  let exReg_3 = /(^[\D]*){1,50}$/g;
  let numberFactorial = document.getElementById("inputfactorial").value;
  if (numberFactorial === "") {
    return (document.getElementById("box_5").innerHTML =
      "No has ingresado el número");
  }
  if (exReg_3.test(numberFactorial)) {
    return (document.getElementById("box_5").innerHTML =
      "Debes ingresar números, no ingrese letras o simbolos");
  }
  if (Math.sign(parseInt(numberFactorial)) === -1)
    return (document.getElementById("box_5").innerHTML =
      "Debes ingresar números positivos");
  return parseInt(numberFactorial);
};

const wordPalindrome = () => {
  let exReg_5 = /[\s]/g;
  let exReg_4 = /[\W\d]/gi;
  let palindrome = document
    .getElementById("inputpalindrome")
    .value.replace(exReg_5, "")
    .toLowerCase();
  let alReves = palindrome.split("").reverse().join("");
  if (palindrome === "")
    return (document.getElementById("box_6").innerHTML =
      "Debes ingresar la palabra o frase a validar");
  if (exReg_4.test(palindrome))
    return (document.getElementById("box_6").innerHTML =
      "No ingrese números o simbolos, solo palabras o frases");

  palindrome === alReves
    ? (document.getElementById("box_6").innerHTML = "Es Palindromo")
    : (document.getElementById("box_6").innerHTML = "No es Palindromo");
};

const capicuaNumber = () => {
  let capicua = document.getElementById("inputCapicua").value;
  let alRevescapicua = capicua.split("").reverse().join("");
  let exReg = /\D/;
  if (capicua === "")
    return (document.getElementById("box_7").innerHTML =
      "Debes ingresar el número a vlidar");
  if (exReg.test(capicua)) {
    return (document.getElementById("box_7").innerHTML =
      "Debes ingresar un número entero positivos, no ingrese letras o simbolos");
  }
  capicua === alRevescapicua
    ? (document.getElementById("box_7").innerHTML = "Es Capicúa")
    : (document.getElementById("box_7").innerHTML = "No es Capicúa");
};

//^\s*[-]*[0-9]*\s*){1,50}$
