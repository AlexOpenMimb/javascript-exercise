/* Palindromos */
const chargerInput = () => {
  let inputPalin = document.getElementById("input").value;
  let inputPalin_2 = inputPalin.replace((expReg = / /g), "");

  let inputPalin_3 = Array.from(inputPalin_2);

  let mediunWord_2 = inputPalin_3.length - 1;
  let mediunWord_3 = mediunWord_2 / 2;
  let this_is_2 = " ";

  for (let i = 0; i < mediunWord_3; i++) {
    let tester = mediunWord_2 - i;
    if (inputPalin_3[i] === inputPalin_3[tester]) {
      this_is_2 = "Es Palindromo";
    } else {
      this_is_2 = " No es Palindromo";
    }
  }
  document.getElementById("box").innerHTML = this_is_2;
};

/* Palindromo II */

let palindromo = ["a", "m", "a", "n", "e", "c", "e", "r", "a"];

let mediunWord = palindromo.length - 1;

let mediunWord_1 = mediunWord / 2;

let this_is = " ";
let this_is_not = " ";
let first = "";
let second = "";

for (let i = 0; i < mediunWord_1; i++) {
  first += palindromo[i];
  second += palindromo[mediunWord--];
}

if (first === second) {
  this_is = " Es Palindromo";
} else {
  this_is_not = " No es palindromo";
}

console.log(this_is);
console.log(this_is_not);

/* Factorial */

let number_1 = 5;
let number_2 = 5;
let result;
let array_2 = [number_1];
let array = [];

for (let i = 0; i < number_1 - 1; i++) {
  let resul = --number_2;
  array.push(resul);
  result = array[i] * array_2[i];
  array_2.push(result);
}
console.log(result);

/* Factorial */

/* Par Number */

let inpar = [];
let par = [];

let ar = [2, 3, 5, 100, 67, 87, 90, 60, 32, 77, 65, 73, 61];
for (let i = 0; i < ar.length; i++) {
  if (ar[i] % 2 === 0) {
    par.push(ar[i]);
  } else if (ar[i] % 2 !== 0) {
    inpar.push(ar[i]);
  }
}
console.log(par);
console.log(inpar);

/* for (let i=0;i<ar.length;i++){
  if(ar[i]%2=== 0){
    console.log(ar[i]);
    continue
    
  }

} */

/* Prime Number */

const primeNumber = () => {
  let number = document.getElementById("input-number").value;
  console.log(number);
  let it_is_prime;
  if (number % 2 === 0) {
    it_is_prime = "Es un número primo";
  } else {
    it_is_prime = "No es un número primo";
  }
  document.getElementById("box-number").innerHTML = it_is_prime;
};

/* Array */

let a = [3, 2, 1];
let c = [];
console.log(a);
for (let i = a.length; i > 0; i--) {
  let minor = a[0];
  for (let i = 0; i < a.length; i++) {
    if (a[i] < minor) {
      minor = a[i];
    }
  }
  let x = minor;
  c.push(x);
  let p = a.indexOf(x);
  a.splice(p, 1);
}

console.log(c);
